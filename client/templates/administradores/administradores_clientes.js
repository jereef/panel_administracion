Template.administradoresClientes.events({
	'submit form': function(e) {

		
		e.preventDefault();

		var cliente = {
			nombre: $(e.target).find('[name=nombre]').val(),
			apellido: $(e.target).find('[name=apellido]').val(),
			fechadenacimiento: $(e.target).find('[name=fechadenacimiento]').val(),
			direccion: $(e.target).find('[name=direccion]').val(),
			cp: $(e.target).find('[name=cp]').val(),
			ciudad: $(e.target).find('[name=ciudad]').val(),
			provincia: $(e.target).find('[name=provincia]').val(),
			email: $(e.target).find('[name=email]').val(),
			telefono: $(e.target).find('[name=telefono]').val()
		};


		cliente._id = Clientes.insert(cliente);
	}
})

Template.administradoresClientes.helpers({
   clientes: function() {
    return Clientes.find();
  }
});