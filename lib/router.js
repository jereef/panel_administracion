Router.configure({
  layoutTemplate: 'layout',
   loadingTemplate: 'loading',
     notFoundTemplate: 'notFound'
});

Router.route('/clientes', {name: 'clientesMain'});
Router.route('/clientes/home', {name: 'clienteHome'});
Router.route('/clientes/servicios', {name: 'clienteServicios'});
Router.route('/clientes/pago', {name: 'clientePagos'});
Router.route('/clientes/mesa', {name: 'clienteMesa'});
Router.route('/clientes/tienda', {name: 'clienteTienda'});
Router.route('/clientes/perfil', {name: 'clientePerfil'});

Router.route('/administradores', {name: 'administradoresMain'});
Router.route('/administradores/clientes', {name: 'administradoresClientes'});
Router.route('/administradores/proyectos', {name: 'administradoresProyectos'});
Router.route('/administradores/facturas', {name: 'administradoresFacturas'});
Router.route('/administradores/tickets', {name: 'administradoresTickets'});
Router.route('/administradores/tienda', {name: 'administradoresTienda'});

var requireLogin = function() {
  if (! Meteor.user()) {
   if (Meteor.loggingIn()) {
      this.render(this.loadingTemplate);
    } else {
      this.render('accessDenied');
    }
  } else {
    this.next();
  }
}
