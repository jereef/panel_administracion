Clientes = new Mongo.Collection('clientes');

Clientes.allow({
  insert: function(userId, doc) {
    // only allow posting if you are logged in
    return !! userId;
  }
});